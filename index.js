const mysql      = require('mysql');
const request    = require('request');
const HTMLParser = require('node-html-parser');
const log        = require('simple-node-logger').createSimpleLogger('logs.log');

// ##### Configuration ##################
let url;
const minLength   = 1;
const startUrl    = 'https://www.duden.de/rechtschreibung/a_italienische_Praeposition';
const showLogging = ['info', 'error'];
const database    = mysql.createConnection({
    host:     'localhost',
    user:     'root',
    password: 'root',
    database: 'duden-crawler'
});
// ##### Configuration ##################

const startCrawler = () => {
    url = startUrl;

    database.connect(async () => {
        let lastUrl;

        while (url) {
            lastUrl = url;
            await makeRequest().then(word => {
                createLog('info', word, 'added', lastUrl);
            }).catch(error => {
                createLog('error', error.word, error.error.message, lastUrl);
            });
        }
    });
};

const makeRequest = () => new Promise((resolve, reject) => {
    let object, word, root;

    return request(url, null, (error, result, body) => {
        return new Promise((resolve, reject) => {
            root   = HTMLParser.parse(body);
            word   = getEncodedWord(root);
            object = createDatabaseObject(root, word);
            url    = object.url;

            if (word.length <= minLength) {
                return reject({ error: { message: 'too small' }, word: word });
            }

            wordUnique({ word: word }).then(() => {

                insertWord(object).then(() => {
                    resolve(word);
                }).catch(error => {
                    reject({ error: error, word: word });
                });

            }).catch(error => {
                reject({ error: error, word: word });
            });

        }).then(word => {
            resolve(word);
        }).catch(error => {
            reject(error);
        });
    });
});

const createDatabaseObject = (root, word) => {
    let dbObject, last, next, freq;

    last = getLastContent(root);
    next = getNextWord(last);
    url  = getUrl(next);
    freq = getFrequency(root);

    dbObject = {
        word:      word,
        url:       url,
        frequency: freq,
    };

    return dbObject;
};

const getEncodedWord = (root) => {
    let word, encodedWord;
    word        = root.querySelector('#block-system-main h1').rawText;
    word        = word.split(',')[0].replace(/\u00AD/g, '');
    encodedWord = decodeURIComponent(word);

    return encodedWord;
};

const getLastContent = (root) => {
    return root.querySelector('#content')
        .lastChild
        .childNodes[1];
};

const getNextWord = (content) => {
    return content.childNodes[1] ?
        content.childNodes[1] :
        content.childNodes[0];
};

const getUrl = (next) => {
    return next
        .childNodes[1]
        .childNodes[1]
        .childNodes[1]
        .attributes.href;
};

const getFrequency = (root) => {
    let freqContainer = root.querySelector('#block-system-main .entry')
        .lastChild
        .lastChild;

    if (freqContainer.childNodes[1]) {
        return freqContainer
            .childNodes[1]
            .rawText
            .length;
    }

    return null;
};

const wordUnique = word => new Promise((resolve, reject) => {
    database.query('SELECT word FROM words WHERE ?', word, (error, result) => {
        if (error) {
            reject(error);
        }
        result[0] ? reject({ message: 'already exists' }) : resolve();
    });
});

const insertWord = word => new Promise((resolve, reject) => {
    database.query('INSERT INTO words SET ?', word, (error) => {
        error ? reject(error) : resolve();
    });
});

const createLog = (type, word, message, url) => {
    if (showLogging.indexOf(type) < 0) {
        return;
    }

    let string = word + getTabs(word) + ' ' + message + getTabs(message) + ' ' + url;

    switch (type) {
        case 'info':
            log.info(string);
            break;
        case 'error':
            log.error(string);
            break;
        default:
            log.error('LOG TYPE NOT FOUND');
    }
};

const getTabs = (string) => {
    let spaces = '';

    while (string.length + spaces.length <= 20) {
        spaces += ' ';
    }
    return spaces;
};

startCrawler();